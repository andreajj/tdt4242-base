from django.test import TestCase, Client
from rest_framework.serializers import ValidationError
from django.core.exceptions import ValidationError as DjangoValidationError
from mock import patch, MagicMock
import json
from users.models import User
from users.serializers import UserSerializer, OfferSerializer


class UserSerializerTestCase(TestCase):
    def setUp(self):
        self.serializer_data = {
            "username": "test",
            "email": "test@example.com",
            "password": "T3stP@assword",
            "password1": "T3stP@assword",
            "athletes": {},
            "workouts": {},
            "coach_files": {},
            "athlete_files": {},
            "phone_number": 1231231,
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "here",
        }
        self.context = MagicMock()

    def test_contains_expected_fields(self):
        serializer = UserSerializer(data=self.serializer_data)

        expected_fields = [
            "url",
            "id",
            "email",
            "username",
            "password",
            "password1",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]
        # Check if the serialized item contains the expected keys
        serializer_fields = list(serializer.fields.keys())
        self.assertEqual(serializer_fields, expected_fields)

    def test_is_valid(self):
        serializer = UserSerializer(data=self.serializer_data)
        self.assertTrue(serializer.is_valid())

    def test_validated_correct_password(self):
        """
            The validate_password function is very weird in a way that it takes in a value (string)
            but it isnt used for any validation. Instead the data given to UserSerializer at creation
            is used. This this is where you have to provide a value to check if it works properly
            and then you can just give validate_password any value you want. If it is successful
            the value given in will be returnt. If it isnt then an exception is raised
        """
        serializer = UserSerializer(data=self.serializer_data)
        try:
            serializer.validate_password("")
        except ValidationError:
            self.fail("validate_password raised ValidationError unexpectedly!")

    """
    def test_validated_wrong_password(self):
        test_data_with_wrong_password = {
            'username': 'test',
            'email': 'test@example.com',
            'password': '',
            'password1': '',
            'athletes': {},
            'workouts': {},
            'coach_files': {},
            'athlete_files': {},
            'phone_number': 1231231,
            'country': 'Norway',
            'city': 'Trondheim',
            'street_address': 'here',
        }
        serializer = UserSerializer(data = test_data_with_wrong_password)
        self.assertRaises(ValidationError, serializer.validate_password, '')
    """

    def test_create(self):
        # Create a user
        test_user = UserSerializer.create(
            self=UserSerializer, validated_data=self.serializer_data
        )

        # Assert the created user has the correct values
        self.assertEqual(test_user.username, self.serializer_data["username"])
        self.assertEqual(test_user.email, self.serializer_data["email"])
        self.assertEqual(test_user.phone_number, self.serializer_data["phone_number"])
        self.assertEqual(test_user.country, self.serializer_data["country"])
        self.assertEqual(test_user.city, self.serializer_data["city"])
        self.assertEqual(
            test_user.street_address, self.serializer_data["street_address"]
        )

        # Passwords are encrypted. If these are equal it means the serializer returns the plaintext password
        self.assertNotEqual(test_user.password, self.serializer_data["password"])


class ScopeCorrectionTestCase(TestCase):
    def setUp(self):
        test_user = User.objects.create(username = "test_user")
        test_user.set_password('test')
        test_user.save()
        data = self.client.post('/api/token/', content_type='application/json',data = {'username': 'test_user', 'password': 'test'}).content.decode('utf8')
        text = json.loads(data)
        self.refresh = text['refresh']
        self.header = {'HTTP_AUTHORIZATION': 'Bearer ' + text['access']}
        self.client = Client(**self.header)
        self.client.login(username='test_user', password='test')


    def test_api_token(self):
        response = self.client.post("/api/token/",data={'username': 'test_user', 'password': 'test'},**self.header)
        self.assertEquals(response.status_code, 200)

    def test_api_token_refresh(self):
        response = self.client.post("/api/token/refresh/",data={'refresh': self.refresh},**self.header)
        self.assertEquals(response.status_code, 200)

    def test_api_remember_me(self):
        response = self.client.get("/api/remember_me/",**self.header)
        self.assertEquals(response.status_code, 200)
    

class OfferModelTestCase(TestCase):
    def test_offers_model_works(self):
        user_serializer = UserSerializer()
        test_athlete = user_serializer.create(validated_data={
                "username": "athlete",
                "email": "test@example.com",
                "password": "T3stP@assword",
                "password1": "T3stP@assword",
                "athletes": {},
                "workouts": {},
                "coach_files": {},
                "athlete_files": {},
                "phone_number": 1231231,
                "country": "Norway",
                "city": "Trondheim",
                "street_address": "here",
            }
        )
        test_coach = user_serializer.create(validated_data={
                "username": "coach",
                "email": "test@example.com",
                "password": "T3stP@assword",
                "password1": "T3stP@assword",
                "athletes": {},
                "workouts": {},
                "coach_files": {},
                "athlete_files": {},
                "phone_number": 1231231,
                "country": "Norway",
                "city": "Trondheim",
                "street_address": "here",
            }
        )
        offer_serializer = OfferSerializer()
        pending_offer = offer_serializer.create(validated_data={
            "owner": test_athlete,
            "recipient": test_coach,
            "status": "p"
        })
        pending_offer.full_clean()
        accepted_offer = offer_serializer.create(validated_data={
            "owner": test_athlete,
            "recipient": test_coach,
            "status": "a"
        })
        pending_offer.full_clean()
        declined_offer = offer_serializer.create(validated_data={
            "owner": test_athlete,
            "recipient": test_coach,
            "status": "d"
        })
        declined_offer.full_clean()

        self.assertEqual(pending_offer.status, "p")
        self.assertEqual(pending_offer.owner, test_athlete)
        self.assertEqual(pending_offer.recipient, test_coach)

        self.assertEqual(accepted_offer.status, "a")
        self.assertEqual(accepted_offer.owner, test_athlete)
        self.assertEqual(accepted_offer.recipient, test_coach)

        self.assertEqual(declined_offer.status, "d")
        self.assertEqual(declined_offer.owner, test_athlete)
        self.assertEqual(declined_offer.recipient, test_coach)

        wrong_offer = offer_serializer.create(validated_data={
            "owner": test_athlete,
            "recipient": test_coach,
            "status": "x"
        })
        self.assertRaises(DjangoValidationError, wrong_offer.full_clean)