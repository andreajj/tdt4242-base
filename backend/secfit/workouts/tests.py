"""
Tests for the workouts application.
"""
from django.test import TestCase, Client
from mock import patch, MagicMock
import datetime
import json
from django.core.files.uploadedfile import SimpleUploadedFile
from workouts.permissions import (
    IsOwner,
    IsOwnerOfWorkout,
    IsCoachAndVisibleToCoach,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsPublic,
    IsWorkoutPublic,
    IsReadOnly,
)
from users.models import User, AthleteFile, Offer
from workouts.models import (
    Workout,
    Exercise,
    ExerciseFile,
    ExerciseInstance,
    WorkoutFile,
)
from workouts.serializers import ExerciseSerializer, ExerciseFileSerializer

# ******************** START Permissions ********************
class PermissionTestCase(TestCase):
    def setUp(self):
        # Create class global objects that can be used in multilpe tests
        self.requester = MagicMock()
        self.request = MagicMock(user=self.requester)
        self.view = MagicMock()
        self.owner = MagicMock()
        self.workout = MagicMock(owner=self.owner)
        self.public_workout = MagicMock(owner=self.owner, visibility="PU")
        self.private_workout = MagicMock(owner=self.owner, visibility="PR")
        self.coach_workout = MagicMock(owner=self.owner, visibility="CO")
        self.workoutFile = MagicMock(workout=self.workout, owner=self.owner)

    def test_IsOwner(self):
        # Permission class to test
        permission = IsOwner()
        # The workoutfile owned by the requester
        workoutFile = MagicMock(workout=self.workout, owner=self.requester)
        # Test that the pemission function works as expected
        self.assertTrue(
            permission.has_object_permission(self.request, self.view, workoutFile)
        )
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, self.workoutFile)
        )

    def test_IsOwnerOfWorkout(self):
        # Permission class to test
        permission = IsOwnerOfWorkout()
        # Create object with owner of workout is the requester
        owned_workout = MagicMock(owner=self.requester)
        owned_obj = MagicMock(workout=owned_workout)
        # Create object with owner of workout not requester
        not_owned_objc = MagicMock(workout=self.workout)
        # Test that the pemission function works as expected
        self.assertTrue(
            permission.has_object_permission(self.request, self.view, owned_obj)
        )
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, not_owned_objc)
        )

    def test_test_IsOwnerOfWorkout_has_permission(self):
        # Permission class to test
        permission = IsOwnerOfWorkout()
        # The database users and workout objects needed for the has_permission test
        test_owner = User.objects.create(
            username="test",
            email="test@test.com",
        )
        test_person = User.objects.create(
            username="person",
            email="person@test.com",
        )
        test_workout = Workout.objects.create(
            name="test", date=datetime.datetime.now(), notes="test", owner=test_owner
        )
        # Requests objects for different scenarios
        post_request_owner = MagicMock(
            user=test_owner,
            method="POST",
            data={
                "workout": "http://localhost:8080/api/workouts/{}/".format(
                    test_workout.id
                )
            },
        )
        post_request_not_owner = MagicMock(
            user=test_person,
            method="POST",
            data={
                "workout": "http://localhost:8080/api/workouts/{}/".format(
                    test_workout.id
                )
            },
        )
        post_request_no_data = MagicMock(user=test_owner, method="POST", data={})
        get_request = MagicMock(
            method="GET",
            data={
                "workout": "http://localhost:8080/api/workouts/{}/".format(
                    test_workout.id
                )
            },
        )
        # User should be allowed to POST to their own workout
        self.assertTrue(permission.has_permission(post_request_owner, self.view))
        # User should not be allowed to POST to someone elses workout
        self.assertFalse(permission.has_permission(post_request_not_owner, self.view))
        # Anyone should be allowed to GET a workout
        self.assertTrue(permission.has_permission(get_request, self.view))
        # It should disallow if it doesnt contian a workout to POST
        self.assertFalse(permission.has_permission(post_request_no_data, self.view))

    def test_IsCoachAndVisibleToCoach(self):
        # Permission class to test
        permission = IsCoachAndVisibleToCoach()
        # Create an object with the coach as the requester
        owner = MagicMock(coach=self.requester)
        obj = MagicMock(owner=owner)

        # Create an object with the coach as a user not the requester
        new_coach = MagicMock()
        not_requester_owner = MagicMock(coach=new_coach)
        not_requester_obj = MagicMock(owner=not_requester_owner)
        # Test that the pemission function works as expected
        self.assertTrue(permission.has_object_permission(self.request, self.view, obj))
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, not_requester_obj)
        )

    def test_IsCoachOfWorkoutAndVisibleToCoach(self):
        # Permission class to test
        permission = IsCoachOfWorkoutAndVisibleToCoach()
        # Create an object where the owner of the workout has a coach which is the requester
        owner = MagicMock(coach=self.requester)
        workout = MagicMock(owner=owner)
        obj = MagicMock(workout=workout)
        # Create an object with a workout, with an owner, who has a coach that is not the requester
        new_coach = MagicMock()
        not_requester_owner = MagicMock(coach=new_coach)
        not_requester_workout = MagicMock(owner=not_requester_owner)
        not_requester_obj = MagicMock(workout=not_requester_workout)
        # Test that the pemission function works as expected
        self.assertTrue(permission.has_object_permission(self.request, self.view, obj))
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, not_requester_obj)
        )

    def test_IsPublic(self):
        # Permission class to test
        permission = IsPublic()
        # Test that the pemission function works as expected
        self.assertTrue(
            permission.has_object_permission(
                self.request, self.view, self.public_workout
            )
        )
        self.assertFalse(
            permission.has_object_permission(
                self.request, self.view, self.private_workout
            )
        )
        self.assertFalse(
            permission.has_object_permission(
                self.request, self.view, self.coach_workout
            )
        )

    def test_IsWorkoutPublic(self):
        # Permission class to test
        permission = IsWorkoutPublic()
        # Create objects where their workout is public, private, and coach
        obj_public = MagicMock(workout=self.public_workout)
        obj_private = MagicMock(workout=self.private_workout)
        obj_coach = MagicMock(workout=self.coach_workout)
        # Test that the pemission function works as expected
        self.assertTrue(
            permission.has_object_permission(self.request, self.view, obj_public)
        )
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, obj_private)
        )
        self.assertFalse(
            permission.has_object_permission(self.request, self.view, obj_coach)
        )

    def test_IsReadOnly(self):
        # Permission class to test
        permission = IsReadOnly()
        # Different types of request methods
        GET_request = MagicMock(method="GET")
        HEAD_request = MagicMock(method="HEAD")
        OPTIONS_request = MagicMock(method="OPTIONS")
        POST_request = MagicMock(method="POST")
        PUT_request = MagicMock(method="POST")
        PATCH_request = MagicMock(method="POST")
        INVALID_request = MagicMock(method="sdfsdf")
        # Check that the 3 safe methods are true
        self.assertTrue(
            permission.has_object_permission(GET_request, self.view, self.workout)
        )
        self.assertTrue(
            permission.has_object_permission(HEAD_request, self.view, self.workout)
        )
        self.assertTrue(
            permission.has_object_permission(OPTIONS_request, self.view, self.workout)
        )
        # Check that unsafe methods are false
        self.assertFalse(
            permission.has_object_permission(POST_request, self.view, self.workout)
        )
        self.assertFalse(
            permission.has_object_permission(PUT_request, self.view, self.workout)
        )
        self.assertFalse(
            permission.has_object_permission(PATCH_request, self.view, self.workout)
        )
        self.assertFalse(
            permission.has_object_permission(INVALID_request, self.view, self.workout)
        )


# ********************* END Permissions *********************


class ExerciseUploadTestCase(TestCase):
    def setUp(self):
        test_exercise = Exercise.objects.create(
            name="test_no_file", description="test", unit="reps"
        )
        Exercise.objects.create(name="test_w_file", description="test", unit="reps")
        url = "http://localhost:8080/api/exercises/" + str(test_exercise.id) + "/"
        self.exercise_serializer_data = {
            "name": "test",
            "description": "test",
            "unit": "reps",
        }
        self.exercise_file_serializer_data = {
            "owner": "test",
            "exercise": url,
            "file": MagicMock(),
        }

    def test_default_exercise_creation(self):
        testExercise = Exercise.objects.get(name="test_no_file")
        self.assertTrue(testExercise.name == "test_no_file")
        self.assertTrue(testExercise.description == "test")
        self.assertTrue(testExercise.unit == "reps")

    def test_file_upload_exercise(self):
        testExercise = Exercise.objects.get(name="test_w_file")
        self.assertTrue(testExercise.name == "test_w_file")
        self.assertTrue(testExercise.description == "test")
        self.assertTrue(testExercise.unit == "reps")

    def test_exercise_serializer(self):
        exercise_serializer = ExerciseSerializer(data=self.exercise_serializer_data)
        self.assertTrue(exercise_serializer.is_valid())
        test_exercise = exercise_serializer.create(
            validated_data=self.exercise_serializer_data
        )
        self.assertEqual(test_exercise.name, self.exercise_serializer_data["name"])
        self.assertEqual(
            test_exercise.description, self.exercise_serializer_data["description"]
        )
        self.assertEqual(test_exercise.unit, self.exercise_serializer_data["unit"])

    def test_exercise_file_serializer(self):
        exercise_serializer = ExerciseSerializer(data=self.exercise_serializer_data)
        self.assertTrue(exercise_serializer.is_valid())
        exercise_file_serializer = ExerciseFileSerializer(
            data=self.exercise_file_serializer_data
        )
        exercise_file_serializer.is_valid()
        self.assertTrue(exercise_file_serializer.is_valid())

    def test_exercise_path(self):
        test_user = User.objects.create(username="test_user")
        test_user.set_password("test")
        test_user.save()
        data = self.client.post(
            "/api/token/",
            content_type="application/json",
            data={"username": "test_user", "password": "test"},
        ).content.decode("utf8")
        text = json.loads(data)
        header = {"HTTP_AUTHORIZATION": "Bearer " + text["access"]}
        self.client = Client(**header)
        self.client.login(username="test_user", password="test")
        response = self.client.post(
            "/api/exercises/", data=self.exercise_serializer_data, **header
        )
        self.assertEquals(response.status_code, 201)

    def test_exercise_file_path(self):
        test_user = User.objects.create(username="test_user")
        test_user.set_password("test")
        test_user.save()
        data = self.client.post(
            "/api/token/",
            content_type="application/json",
            data={"username": "test_user", "password": "test"},
        ).content.decode("utf8")
        text = json.loads(data)
        header = {"HTTP_AUTHORIZATION": "Bearer " + text["access"]}
        self.client = Client(**header)
        self.client.login(username="test_user", password="test")
        response = self.client.get("/api/exercise-files/")
        self.assertEquals(response.status_code, 200)

    def test__exercise_instance_path(self):
        test_user = User.objects.create(username="test_user")
        test_user.set_password("test")
        test_user.save()

        exercise_serializer = ExerciseSerializer(data=self.exercise_serializer_data)
        self.assertTrue(exercise_serializer.is_valid())
        test_exercise = exercise_serializer.create(
            validated_data=self.exercise_serializer_data
        )

        data = self.client.post(
            "/api/token/",
            content_type="application/json",
            data={"username": "test_user", "password": "test"},
        ).content.decode("utf8")
        text = json.loads(data)
        header = {"HTTP_AUTHORIZATION": "Bearer " + text["access"]}
        self.client = Client(**header)
        self.client.login(username="test_user", password="test")
        response = self.client.get("/api/exercises/" + str(test_exercise.id) + "/")
        self.assertEquals(response.json()["name"], "test")
        self.assertEquals(response.status_code, 200)

    def test_exercise_file_upload(self):
        test_user = User.objects.create(username="test_user")
        test_user.set_password("test")
        test_user.save()
        data = self.client.post(
            "/api/token/",
            content_type="application/json",
            data={"username": "test_user", "password": "test"},
        ).content.decode("utf8")
        text = json.loads(data)
        header = {"HTTP_AUTHORIZATION": "Bearer " + text["access"]}
        self.client = Client(**header)
        self.client.login(username="test_user", password="test")
        video = SimpleUploadedFile(
            "file.mp4", b"file_content", content_type="video/mp4"
        )
        info = self.exercise_serializer_data
        info["files"] = [video]
        response = self.client.post("/api/exercises/", data=info, **header)
        self.assertEquals(response.status_code, 201)
        exercise_file_id = response.json()["files"][0]["id"]
        check_for_exercise_file = self.client.get(
            "/api/exercise-files/" + str(exercise_file_id) + "/", **header
        )
        self.assertEquals(check_for_exercise_file.status_code, 200)
