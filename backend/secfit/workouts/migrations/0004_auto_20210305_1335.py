# Generated by Django 3.1 on 2021-03-05 12:35

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ("workouts", "0003_rememberme"),
    ]

    operations = [
        migrations.AddField(
            model_name="workout",
            name="time",
            field=models.TimeField(
                default=datetime.datetime(2021, 3, 5, 12, 35, 13, 985056, tzinfo=utc)
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="workout",
            name="date",
            field=models.DateField(),
        ),
    ]
