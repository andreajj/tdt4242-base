# Generated by Django 3.1 on 2021-03-05 15:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("workouts", "0006_remove_workout_time"),
        ("workouts", "0005_remove_exercisefile_owner"),
    ]

    operations = []
