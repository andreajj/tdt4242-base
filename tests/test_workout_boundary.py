import unittest
import uuid
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC

from .constants import DEFAULT_WAIT, TEST_ROOT, TEST_USER_USERNAME, TEST_USER_PASSWORD
from .helpers.registration import write_registration_inputs
from .helpers.login import log_in
from .helpers.rest import get_user_tokens, create_workout, get_workout, add_coach, get_current_user


class ElementsReadOnly(object):
    """
    source https://sqa.stackexchange.com/questions/7018/how-to-wait-until-the-count-of-a-web-element-changes-in-webdriver
    An expectation for checking that an elements has changes.

    locator - used to find the element
    returns the WebElement once the length has changed
    """

    def __init__(self, locator):
        self.locator = locator

    def __call__(self, driver):
        element = driver.find_element(*self.locator)
        if element.get_attribute("readonly") == None:
            return element
        else:
            return False


class InitTest(unittest.TestCase):
    # initialization of webdriver 

    min_name_invalid = ""
    min_name_valid = "a"
    min_name_safe = "aa"

    max_name_invalid = "101characters101characters101characters101characters101characters101characters101characters101charact"
    max_name_valid = "100characters100characters100characters100characters100characters100characters100characters100charac"
    max_name_safe = "99characters99characters99characters99characters99characters99characters99characters99characters99c"

    min_date_day_valid = "01/01/2020"
    min_date_day_safe = "01/02/2020"

    min_date_month_valid = "01/01/2020"
    min_date_month_safe = "02/01/2020"

    min_date_year_valid = "01/01/0000"
    min_date_year_safe = "01/01/0001"

    max_date_day_long_month_valid = "01/31/2020"
    max_date_day_long_month_safe = "01/30/2020"

    max_date_day_short_month_valid = "11/30/2020"
    max_date_day_short_month_safe = "11/29/2020"

    max_date_day_short_february_valid = "02/29/2020"
    max_date_day_short_february_safe = "02/28/2020"

    max_date_month_valid = "12/01/2020"
    max_date_month_safe = "11/01/2020"

    max_date_year_valid = "01/01/9999"
    max_date_year_safe = "01/01/9998"

    min_time_minute_valid = "00:00"
    min_time_minute_safe = "00:01"

    min_time_hour_valid = "00:00"
    min_time_hour_safe = "01:00"

    max_time_minute_valid = "23:59"
    max_time_minute_safe = "23:58"

    max_time_hour_valid = "23:00"
    max_time_hour_safe = "23:00"

    min_notes_invalid = ""
    min_notes_valid = "a"
    min_notes_safe = "aa"

    min_exercise_sets_valid = "0"
    min_exercise_sets_safe = "1"

    min_exercise_number_valid = "0"
    min_exercise_number_safe = "1"

    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.ensure_user_created()

    def ensure_user_created(self):
        self.driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(self.driver,
                                  TEST_USER_USERNAME,
                                  "test@test.test",
                                  TEST_USER_PASSWORD,
                                  TEST_USER_PASSWORD,
                                  "",
                                  "日本",
                                  "北海道",
                                  "まんこ通り")

    def ensure_login(self):
        self.driver.get("%s/login.html" % TEST_ROOT)
        self.write_to_input("username", TEST_USER_USERNAME)
        self.write_to_input("password", TEST_USER_PASSWORD)
        submit_button = self.driver.find_element(By.ID, "btn-login")
        submit_button.click()
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.title_is("Workouts"))

    # Helper function to find input

    def write_to_input(self, input_name, text):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.NAME, input_name)))

        locator = (By.NAME, input_name)
        condition = ElementsReadOnly(locator)
        WebDriverWait(self.driver, 10).until(condition)


        if (text == None):
            self.driver.find_element(By.NAME, input_name).send_keys(Keys.RETURN)
        else:
            self.driver.find_element(By.NAME, input_name).send_keys(text + Keys.RETURN)

    def write_inputs(
            self,
            name="name",
            date="02/02/2020",
            time="23:23",
            visibility="Public",
            notes="Note",
    ):
        # This is needed to make sure duplicated names dont throw error

        self.write_to_input("name", name)
        self.write_to_input("date", date)
        self.write_to_input("time", time)
        self.write_to_input("notes", notes)
        self.submit()

    def write_exercise_inputs(
            self,
            exercise_type="Push-up",
            sets="5",
            number="2",
    ):
        # This is needed to make sure duplicated names dont throw error
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.NAME, "type")))

        self.write_to_input("type", exercise_type)
        self.write_to_input("sets", sets)
        self.write_to_input("number", number)

    def assert_successful_workout(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_element_located((By.XPATH, "//li[contains(text(),'Successfully created a workout')]")))

    def assert_failed_workout(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_element_located((By.XPATH, "//strong[contains(text(),'Could not create new workout!')]")))

    # Helper function to sumbit form
    def submit(self):
        submit_button = self.driver.find_element(By.ID, "btn-ok-workout")
        submit_button.click()

    def tearDown(self):
        self.driver.close()


class Name(InitTest):

    def test_max_valid_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().max_name_safe)
        super().assert_successful_workout()

    def test_max_valid_name(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().max_name_valid)
        super().assert_successful_workout()

    def test_max_invalid_name(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().max_name_invalid)
        super().assert_failed_workout()

    def test_min_valid_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().min_name_safe)
        super().assert_successful_workout()

    def test_min_valid_name(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().min_name_valid)
        super().assert_successful_workout()

    def test_min_invalid_name(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(name=super().min_name_invalid)
        super().assert_failed_workout()


class Date(InitTest):
    """
    For this boundary test we decided against strict boundary validation,
    as the component truncated values outside the scope. The tests would be testing boundaries beyond intended use, as things like dates 32-99 were truncated down to 31 etc.
    """

    def test_min_date_day_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_day_valid)
        super().assert_successful_workout()

    def test_min_date_day_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_day_valid)
        super().assert_successful_workout()

    def test_min_date_month_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_month_safe)
        super().assert_successful_workout()

    def test_min_date_month_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_month_valid)
        super().assert_successful_workout()

    def test_min_date_year_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_year_safe)
        super().assert_successful_workout()

    def test_min_date_year_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().min_date_year_valid)
        super().assert_successful_workout()

    def test_max_date_day_long_month_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_long_month_safe)
        super().assert_successful_workout()

    def test_max_date_day_long_month_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_long_month_valid)
        super().assert_successful_workout()

    def test_max_date_day_short_month_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_short_month_safe)
        super().assert_successful_workout()

    def test_max_date_day_short_month_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_short_month_valid)
        super().assert_successful_workout()

    def test_max_date_day_short_february_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_short_february_safe)
        super().assert_successful_workout()

    def test_max_date_day_short_february_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_day_short_february_valid)
        super().assert_successful_workout()

    def test_max_date_month_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_month_safe)
        super().assert_successful_workout()

    def test_max_date_month_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_month_valid)
        super().assert_successful_workout()

    def test_max_date_year_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_year_safe)
        super().assert_successful_workout()

    def test_max_date_year_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(date=super().max_date_year_valid)
        super().assert_successful_workout()


class Time(InitTest):
    """
    For this boundary test we decided against strict boundary validation.
    The DateTimeField truncates entries outside the expected boundaries, and testing outlier values far outside the designed boundaries goes against boundary testing.
    """

    def test_min_time_minute_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().min_time_minute_safe)
        super().assert_successful_workout()

    def test_min_time_minute_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().min_time_minute_valid)
        super().assert_successful_workout()

    def test_min_time_hour_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().min_time_hour_safe)
        super().assert_successful_workout()

    def test_min_time_hour_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().min_time_hour_valid)
        super().assert_successful_workout()

    def test_max_time_minute_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().max_time_minute_safe)
        super().assert_successful_workout()

    def test_max_time_minute_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().max_time_minute_valid)
        super().assert_successful_workout()

    def test_max_time_hour_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().max_time_hour_safe)
        super().assert_successful_workout()

    def test_max_time_hour_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(time=super().max_time_hour_valid)
        super().assert_successful_workout()


class Notes(InitTest):
    def test_min_notes_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(notes=super().min_notes_safe)
        super().assert_successful_workout()

    def test_min_notes_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(notes=super().min_notes_valid)
        super().assert_successful_workout()

    def test_min_notes_invalid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_inputs(notes=super().min_notes_invalid)
        super().assert_failed_workout()


class Exercises(InitTest):
    def test_min_sets_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_exercise_inputs(sets=super().min_exercise_sets_safe)
        super().write_inputs()
        super().assert_successful_workout()

    def test_min_sets_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_exercise_inputs(sets=super().min_exercise_sets_valid)
        super().write_inputs()
        super().assert_successful_workout()

    def test_min_number_safe(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_exercise_inputs(number=super().min_exercise_number_safe)
        super().write_inputs()
        super().assert_successful_workout()

    def test_min_number_valid(self):
        super().ensure_login()
        self.driver.get("http://localhost:3000/workout.html")
        super().write_exercise_inputs(number=super().min_exercise_number_valid)
        super().write_inputs()

        super().assert_successful_workout()
