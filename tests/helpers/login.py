from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

from ..constants import DEFAULT_WAIT, TEST_ROOT

VISIBILITY_PUBLIC = "PU"
VISIBILITY_COACH = "CO"
VISIBILITY_PRIVATE = "PR"


def log_in(driver, username, password):
    # Log in
    driver.get("%s/login.html" % TEST_ROOT)
    username_input = WebDriverWait(driver, timeout=DEFAULT_WAIT).until(lambda d: d.find_element(By.NAME, "username"))
    assert username_input is not None
    password_input = WebDriverWait(driver, timeout=DEFAULT_WAIT).until(lambda d: d.find_element(By.NAME, "password"))
    assert password_input is not None

    username_input.send_keys(username);
    password_input.send_keys(password);

    loginBtn = driver.find_element(By.ID, "btn-login")
    assert loginBtn
    loginBtn.click()

    assert_logged_in(driver)

def assert_logged_in(driver):
    successToast = WebDriverWait(driver, timeout=DEFAULT_WAIT).until(lambda d: d.find_element(By.XPATH, "//*[contains(text(), 'Welcome back!')]"))
    assert successToast