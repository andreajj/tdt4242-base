from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from . import write_to_input

def write_registration_inputs(
    driver, 
    username, 
    email, 
    password,
    password1,
    phone,
    country,
    city,
    address
):
    write_to_input(driver, "username", username)
    write_to_input(driver, "email", email)
    write_to_input(driver, "password", password)
    write_to_input(driver, "password1", password1)
    write_to_input(driver, "phone_number", phone)
    write_to_input(driver, "country", country)
    write_to_input(driver, "city", city)
    write_to_input(driver, "street_address", address)
    submit(driver)

def assert_successful_registration(driver):
    wait = WebDriverWait(driver, 5)
    wait.until(EC.title_is("Workouts"))

def assert_failed_registration(driver):
    wait = WebDriverWait(driver, 5)
    wait.until(EC.visibility_of_element_located((By.XPATH, "//strong[contains(text(),'Registration failed!')]")))

# Helper function to sumbit form
def submit(driver):
    submit_button = driver.find_element(By.ID, "btn-create-account")
    submit_button.click()
