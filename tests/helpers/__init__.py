from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

def write_to_input(driver, input_name, text):
    if (text == None):
        driver.find_element(By.NAME, input_name).send_keys(Keys.RETURN)
    else:
        driver.find_element(By.NAME, input_name).send_keys(text + Keys.RETURN)
