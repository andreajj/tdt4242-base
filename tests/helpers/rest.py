import requests 
import json

from ..constants import REST_API_BASE

""" 
By logging in with the REST api, we can get more details about the objects we create, 
which lets us write more intelligent tests that better handle the unstable database of local development
"""

def get_user_tokens(username, password):
	payload = {
		'username': username,
		'password': password
	}
	result = requests.post("%s/token/" % REST_API_BASE, data=payload).json()

	return result

def create_workout(token, name, notes, datetime, visibility, file):
	payload = {
		'name': name,
		'notes': notes,
		'date': datetime.isoformat(),
		'visibility': visibility,
		'exercise_instances': '[]'
	}

	files={
		'files': ('file.txt', file)
	}

	headers={
		'authorization': "Bearer %s" % token
	}

	return requests.post("%s/workouts/" % REST_API_BASE, data=payload, files=files, headers=headers).json()

def get_workout(token, workout_id):
	headers={
		'authorization': "Bearer %s" % token
	}

	return requests.get("%s/workouts/%d/" % (REST_API_BASE, workout_id), headers=headers).json()

def get_workouts(token):
	headers={
		'authorization': "Bearer %s" % token
	}

	return requests.get("%s/workouts/" % REST_API_BASE, headers=headers).json()

def add_coach(token, my_id, coach_url):
	payload = {
		'coach': coach_url
	}

	headers={
		'authorization': "Bearer %s" % token
	}

	result = requests.patch("%s/users/%d/" % (REST_API_BASE, my_id), data=payload, headers=headers).json()

def get_current_user(token):
	headers={
		'authorization': "Bearer %s" % token
	}

	return requests.get("%s/users/?user=current" % REST_API_BASE, headers=headers).json()['results'][0]