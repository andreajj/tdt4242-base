from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

from ..constants import DEFAULT_WAIT

def assert_toast(driver, text):
    successToast = WebDriverWait(driver, timeout=DEFAULT_WAIT*1).until(lambda d: d.find_element(By.XPATH, "//*[contains(text(), '%s')]" % text))
    assert successToast


def assert_not_toast(driver, text):
    try:
        successToast = WebDriverWait(driver, timeout=DEFAULT_WAIT*1).until(lambda d: d.find_element(By.XPATH, "//*[contains(text(), '%s')]" % text))
        assert False
    except Exception as e:
        print("Aaa")
    assert True
