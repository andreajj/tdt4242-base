import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from .constants import TEST_ROOT
from .helpers.registration import write_registration_inputs
from .helpers.rest import get_user_tokens, get_workouts, create_workout
from selenium.webdriver.support import expected_conditions as EC
import uuid
from datetime import datetime

'''
Integration test for UC-1

As this feature is very limited in scope. Only one html page and it's js script file has been affected.
Thus the Coupling is very low and the Cohesion is high. Because of this a call-graph wont be written
And the test itself will mostly be a system test.

'''

class ElementsLengthChanges(object):
    """
    source https://sqa.stackexchange.com/questions/7018/how-to-wait-until-the-count-of-a-web-element-changes-in-webdriver
    An expectation for checking that an elements has changes.

    locator - used to find the element
    returns the WebElement once the length has changed
    """
    def __init__(self, locator, length):
        self.locator = locator
        self.length = length

    def __call__(self, driver):
        element = driver.find_elements(*self.locator)
        element_count = len(element)
        if element_count >= self.length:
            return element
        else:
            return False

class InfinityScroll(unittest.TestCase): 
    # initialization of webdriver 
    def setUp(self): 
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.add_argument('-headless')
        firefox_options.add_argument("--width=1920")
        firefox_options.add_argument("--height=1080")
        self.driver = webdriver.Firefox(firefox_options=firefox_options)

    def scroll_to_bottom(self, driver):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight*0.9);");

    def wait_for_exercises(self, driver, count):
        locator = (By.XPATH, "//a[contains(@class,'workout')]")
        condition = ElementsLengthChanges(locator, count)
        WebDriverWait(driver, 10).until(condition)

    def test(self):
        username = str(uuid.uuid4())
        # Register user
        self.driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(
            self.driver, 
            username, 
            "test@test.test", 
            "test",
            "test",
            "11223344",
            "Norway",
            "Trondheim",
            "My street"
        )

        # Check how many exercises there are, if less than 40 then create
        # the remaining amount
        tokens = get_user_tokens(username, "test")
        workouts = get_workouts(tokens['access'])
        workoutsCount = workouts['count']
        if workoutsCount < 40:
            countToMake =  40 - workoutsCount
            for i in range(countToMake):
                create_workout(tokens['access'], "Test workout(public)", "This is a note", datetime.now(), "PU", "A file!")
        
        # Go to the exercies page
        self.driver.get("%s/workouts.html" % TEST_ROOT)
        self.driver.refresh()

        self.wait_for_exercises(self.driver, 10)
        self.scroll_to_bottom(self.driver)

        self.wait_for_exercises(self.driver, 20)
        self.scroll_to_bottom(self.driver)

        self.wait_for_exercises(self.driver, 30)
        self.scroll_to_bottom(self.driver)

        self.wait_for_exercises(self.driver, 40)

        elements = self.driver.find_elements(By.XPATH, "//a[contains(@class,'workout')]")
        
        assert len(elements) == 40

    def tearDown(self): 
        self.driver.close()
