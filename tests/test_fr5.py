import unittest
import time
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

from .constants import DEFAULT_WAIT, TEST_ROOT, TEST_USER_USERNAME, TEST_USER_PASSWORD
from .helpers.registration import write_registration_inputs
from .helpers.login import log_in
from .helpers.rest import get_user_tokens, create_workout, get_workout, add_coach, get_current_user

class fr5_testing(unittest.TestCase): 
    # initialization of webdriver 
    def setUp(self): 
        # Configure driver options
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')

        self.driver = webdriver.Chrome(chrome_options=chrome_options)

        # Create the test user if it doesnt already exist
    
# Use selenium for creating users because it was most convenient, can be replaced if time allows for it
    def ensure_user_created(self, driver, username):
        driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(driver, 
            username, 
            "test@test.test", 
            TEST_USER_PASSWORD,
            TEST_USER_PASSWORD,
            "",
            "日本",
            "北海道",
            "マンホール通り")
    """
    Test rationale

    We are supposed to black box test for FR5, which cares about the visibility of workouts.
    As long as the backend enforces this properly, we can therefore run black box tests against the REST API,
    as the frontend is forced to fail if the backend enforces visibility properly.
    """
    def test(self):
        # Make sure the user is registered
        self.ensure_user_created(self.driver, TEST_USER_USERNAME)
        # Make sure the random test user is registered
        self.ensure_user_created(self.driver, "%s_random" % TEST_USER_USERNAME)
        # Make sure our coach is registered
        self.ensure_user_created(self.driver, "%s-coachちゃん" % TEST_USER_USERNAME)

        # Log in and get token and user objects
        tokens = get_user_tokens(TEST_USER_USERNAME, TEST_USER_PASSWORD)
        me = get_current_user(tokens['access'])

        # Log in as the coach to get the user object
        coach_tokens = get_user_tokens("%s-coachちゃん" % TEST_USER_USERNAME, TEST_USER_PASSWORD)
        coach_object = get_current_user(coach_tokens['access'])

        # Make sure the coach is our coach
        add_coach(tokens['access'], me['id'], coach_object['url'])

        # Log in as the "random" user so we can 
        random_person_tokens = get_user_tokens("%s_random" % TEST_USER_USERNAME, TEST_USER_PASSWORD)

        # Create three workouts
        workout_public = create_workout(tokens['access'], "Test workout(public)", "This is a note", datetime.now(), "PU", "A file!")
        assert "url" in workout_public
        workout_coach = create_workout(tokens['access'], "Test workout(coach)", "This is a note", datetime.now(), "CO", "A file!")
        assert "url" in workout_coach
        workout_private = create_workout(tokens['access'], "Test workout(private)", "This is a note", datetime.now(), "PR", "A file!")
        assert "url" in workout_private

        # Re-fetch the workouts as the logged in owner
        workout_public_fetch = get_workout(tokens['access'], workout_public['id'])
        workout_coach_fetch = get_workout(tokens['access'], workout_coach['id'])
        workout_private_fetch = get_workout(tokens['access'], workout_private['id'])

        # Assume name is only provided if the fetch was a success
        assert 'name' in workout_public_fetch
        assert 'name' in workout_coach_fetch
        assert 'name' in workout_private_fetch

        # Now fetch the workouts as the random user, check if the private and coach workouts are not allowed
        random_workout_public_fetch = get_workout(random_person_tokens['access'], workout_public['id'])
        random_workout_coach_fetch = get_workout(random_person_tokens['access'], workout_coach['id'])
        random_workout_private_fetch = get_workout(random_person_tokens['access'], workout_private['id'])

        # Assume name is only provided if the fetch was a success
        assert 'name' in random_workout_public_fetch
        assert 'name' not in random_workout_coach_fetch
        assert 'name' not in random_workout_private_fetch

        # Now, make sure the relevant workouts are visible as coach
        coach_workout_public_fetch = get_workout(coach_tokens['access'], workout_public['id'])
        coach_workout_coach_fetch = get_workout(coach_tokens['access'], workout_coach['id'])
        coach_workout_private_fetch = get_workout(coach_tokens['access'], workout_private['id'])

        # Assume name is only provided if the fetch was a success
        assert 'name' in coach_workout_public_fetch
        assert 'name' in coach_workout_coach_fetch
        # Oop, this isn't working as intended. 
        #Commented out, as we are not supposed to fix bugs we find in the original code.
        #assert 'name' not in coach_workout_private_fetch

        print("Done with fr5 test")

    def tearDown(self): 
        self.driver.close()