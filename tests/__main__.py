import unittest

if __name__ == '__main__':
    testsuite = unittest.TestLoader().discover('.')
    result = unittest.TextTestRunner(verbosity=1).run(testsuite)

    if result.wasSuccessful():
        exit(0)
    else:
        exit(1)