import unittest
import uuid
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC

from .constants import DEFAULT_WAIT, TEST_ROOT, TEST_USER_USERNAME, TEST_USER_PASSWORD
from .helpers.registration import write_registration_inputs
from .helpers.login import log_in
from .helpers.rest import get_user_tokens, create_workout, get_workout, add_coach, get_current_user


'''
Integration test for UC-2

This is a feature that was relatively simple, as it simply required us to split an already existing feature into two.
The reason for this was to support more platforms.
This means that we test that these two features work seperately and combine to give the desired result.

'''

class DateAndTimePickers(unittest.TestCase): 
    def setUp(self): 
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.ensure_user_created()

    def ensure_user_created(self):
        self.driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(self.driver, 
            TEST_USER_USERNAME, 
            "test@test.test", 
            TEST_USER_PASSWORD,
            TEST_USER_PASSWORD,
            "",
            "日本",
            "北海道",
            "まんこ通り")

    def ensure_login(self):
        self.driver.get("%s/login.html" % TEST_ROOT)
        self.write_to_input("username",TEST_USER_USERNAME)
        self.write_to_input("password",TEST_USER_PASSWORD)
        submit_button = self.driver.find_element(By.ID, "btn-login")
        submit_button.click()
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.title_is("Workouts"))


     # Helper function to find input
    
    def write_to_input(self, input_name, text):
 
        self.driver.find_element(By.NAME, input_name).send_keys(text)
    
    def submit(self):
        submit_button = self.driver.find_element(By.ID, "btn-ok-workout")
        submit_button.click()

    def write_inputs(
        self, 
        name = "name", 
        notes = "Note",
    ):
        # This is needed to make sure duplicated names dont throw error
        
        self.write_to_input("notes", notes)
        self.write_to_input("name", name)
        self.submit()

    def assert_successful_creation(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//li[contains(text(),'Successfully created a workout')]")))

    def test_date_and_time_correctly_set(self):
        self.ensure_login()
        self.driver.get("http://localhost:3000/workout.html")

        self.assertEquals(self.driver.find_element(By.NAME,"date").get_attribute("value"),"")
        self.write_to_input("date","05/05/2021")
        
        self.write_to_input("time","1015")
        self.assertEquals(self.driver.find_element(By.NAME,"date").get_attribute("value"),"2021-05-05")
        self.write_inputs()
        self.assert_successful_creation()

    def tearDown(self): 
        self.driver.close()