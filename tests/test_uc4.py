import unittest
import uuid
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC

from .constants import DEFAULT_WAIT, TEST_ROOT, TEST_USER_USERNAME, TEST_USER_PASSWORD
from .helpers.registration import write_registration_inputs
from .helpers.login import log_in
from .helpers.rest import get_user_tokens, create_workout, get_workout, add_coach, get_current_user


class ExerciseIllustrationUpload(unittest.TestCase): 
    # initialization of webdriver 

    TEST_IMAGE_FILE_PATH = os.path.abspath('tests/assets/test_image.png')
    print(TEST_IMAGE_FILE_PATH)
    def setUp(self): 
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)
        self.ensure_user_created()
    
# Use selenium for creating users because it was most convenient, can be replaced if time allows for it
    def ensure_user_created(self):
        self.driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(self.driver, 
            TEST_USER_USERNAME, 
            "test@test.test", 
            TEST_USER_PASSWORD,
            TEST_USER_PASSWORD,
            "",
            "日本",
            "北海道",
            "まんこ通り")

    def ensure_login(self):
        self.driver.get("%s/login.html" % TEST_ROOT)
        self.write_to_input("username",TEST_USER_USERNAME)
        self.write_to_input("password",TEST_USER_PASSWORD)
        submit_button = self.driver.find_element(By.ID, "btn-login")
        submit_button.click()
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.title_is("Workouts"))

    def write_to_input(self, input_name, text):
        if (text == None):
            self.driver.find_element(By.NAME, input_name).send_keys(Keys.RETURN)
        else:
            self.driver.find_element(By.NAME, input_name).send_keys(text + Keys.RETURN)
    
    def submit(self):
        submit_button = self.driver.find_element(By.ID, "btn-ok-exercise")
        submit_button.click()

    def assert_successful_generation(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//li[contains(text(),'Successfully created exercise')]")))

    def write_inputs(
        self, 
        name = "name", 
        description = "description", 
        unit = "units",
        files = None     
    ):
        # This is needed to make sure duplicated names dont throw error
        if (name == "name"):
            name = str(uuid.uuid4())

        self.write_to_input("unit", unit)
        self.write_to_input("description", description)
        self.write_to_input("name", name)
        if (files is not None):
            self.driver.find_element(By.NAME, 'files').send_keys(files)

        self.submit()
    """
    Test rationale

    Checking to make sure the exercise generation works and that uploading files successfully generates an exercise.
    """
    def test_no_file(self):
        self.ensure_login()
        self.driver.get("http://localhost:3000/exercise.html")
        self.write_inputs()
        self.assert_successful_generation()

    def test_with_file(self):
        self.ensure_login()
        self.driver.get("http://localhost:3000/exercise.html")
        self.write_inputs(files=self.TEST_IMAGE_FILE_PATH)
        self.assert_successful_generation()

    def tearDown(self): 
        self.driver.close()