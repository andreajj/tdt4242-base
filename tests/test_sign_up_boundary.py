import unittest
import uuid
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC

class InitTest(unittest.TestCase): 
    # initialization of webdriver 

    min_password_invalid = ""
    min_password_valid = "a"
    min_password_safe = "aa"

    max_password_safe = "256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256characters256charac"

    min_name_invalid = ""
    min_name_valid = "a"
    min_name_safe = "aa"

    max_name_invalid = "151characters151characters151characters151characters151characters151characters151characters151characters151characters151characters151characters151chara"
    max_name_valid = "150characters150characters150characters150characters150characters150characters150characters150characters150characters150characters150characters150char"
    max_name_safe = "149characters149characters149characters149characters149characters149characters149characters149characters149characters149characters149characters149cha"


    min_phone_number_valid = ""
    min_phone_number_safe = "a"

    #phone number allows up to 50 digits
    max_phone_number_invalid = "100000000000000000000000000000000000000000000000000"
    max_phone_number_valid = "99999999999999999999999999999999999999999999999999"
    max_phone_number_safe = "99999999999999999999999999999999999999999999999998"

    min_country_valid = ""
    min_country_safe = "a"

    max_country_invalid = "51characters51characters51characters51characters51c"
    max_country_valid = "50characters50characters50characters50characters50"
    max_country_safe = "49characters49characters49characters49characters4"

    min_city_valid = ""
    min_city_safe = "a"

    max_city_invalid = "51characters51characters51characters51characters51c"
    max_city_valid = "50characters50characters50characters50characters50"
    max_city_safe = "49characters49characters49characters49characters4"

    min_street_address_valid = ""
    min_street_address_safe = "a"

    max_street_address_invalid = "51characters51characters51characters51characters51c"
    max_street_address_valid = "50characters50characters50characters50characters50"
    max_street_address_safe = "49characters49characters49characters49characters4"

    def setUp(self): 
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

     # Helper function to find input
    def write_to_input(self, input_name, text):
        if (text == None):
            self.driver.find_element(By.NAME, input_name).send_keys(Keys.RETURN)
        else:
            self.driver.find_element(By.NAME, input_name).send_keys(text + Keys.RETURN)

    def write_inputs(
        self, 
        username = "username", 
        email = "test@test.com", 
        password = "password",
        password1 = "password",
        phone = "12345678",
        country = "Norway",
        city = "Trondheim",
        address = "Address"
    ):
        # This is needed to make sure duplicated usernames dont throw error
        if (username == "username"):
            username = str(uuid.uuid4())
        self.write_to_input("username", username)
        self.write_to_input("email", email)
        self.write_to_input("password", password)
        self.write_to_input("password1", password1)
        self.write_to_input("phone_number", phone)
        self.write_to_input("country", country)
        self.write_to_input("city", city)
        self.write_to_input("street_address", address)
        self.submit()

    def assert_successful_registration(self):
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.title_is("Workouts"))
    

    def assert_username_success_or_taken(self):
        wait = WebDriverWait(self.driver, 5)
        wait.until(lambda driver: driver.find_elements(By.XPATH,"//li[contains(text(),'Successfully registered - welcome!')]") or driver.find_elements(By.XPATH, "//li[contains(text(),'A user with that username already exists.')]"))

    def assert_failed_registration(self):
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//strong[contains(text(),'Registration failed!')]")))

    # Helper function to sumbit form
    def submit(self):
        submit_button = self.driver.find_element(By.ID, "btn-create-account")
        submit_button.click()

    def tearDown(self):
        self.driver.close()
class Username(InitTest):

    def test_max_safe_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().max_name_safe)
        super().assert_username_success_or_taken()
    def test_max_valid_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().max_name_valid)
        super().assert_username_success_or_taken()
    def test_max_invalid_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().max_name_invalid)
        super().assert_failed_registration()

    def test_min_safe_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().min_name_safe)
        super().assert_username_success_or_taken()
    def test_min_valid_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().min_name_valid)
        super().assert_username_success_or_taken()
    def test_min_invalid_username(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username=super().min_name_invalid)
        super().assert_failed_registration()

class Password(InitTest):

    """
    Due to how passwords are stored, there is no realistic upper boundary for password lengths. We are testing to make sure this is still the case in the future.
    """
    def test_max_safe_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password=super().max_password_safe,password1=super().max_password_safe)
        super().assert_successful_registration()

    def test_min_safe_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password=super().min_password_safe,password1=super().min_password_safe)
        super().assert_successful_registration()
    def test_min_valid_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password=super().min_password_valid,password1=super().min_password_valid)
        super().assert_successful_registration()
    def test_min_invalid_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password=super().min_password_invalid,password1=super().min_password_invalid)
        super().assert_failed_registration()


class Phone_Number(InitTest):
    def test_max_safe_phone_number(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone=super().max_phone_number_safe)
        super().assert_successful_registration()
    def test_max_valid_phone_number(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone=super().max_phone_number_valid)
        super().assert_successful_registration()
    def test_max_invalid_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone=super().max_phone_number_invalid)
        super().assert_failed_registration()

    def test_min_safe_phone_number(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone=super().min_phone_number_safe)
        super().assert_successful_registration()
    def test_min_valid_phone_number(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone=super().min_phone_number_valid)
        super().assert_successful_registration()

class Country(InitTest):
    def test_max_safe_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country=super().max_country_safe)
        super().assert_successful_registration()
    def test_max_valid_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country=super().max_country_valid)
        super().assert_successful_registration()
    def test_max_invalid_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country=super().max_country_invalid)
        super().assert_failed_registration()

    def test_min_safe_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country=super().min_country_safe)
        super().assert_successful_registration()
    def test_min_valid_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country=super().min_country_valid)
        super().assert_successful_registration()


class City(InitTest):
    def test_max_safe_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city=super().max_city_safe)
        super().assert_successful_registration()
    def test_max_valid_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city=super().max_city_valid)
        super().assert_successful_registration()
    def test_max_invalid_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city=super().max_city_invalid)
        super().assert_failed_registration()

    def test_min_safe_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city=super().min_city_safe)
        super().assert_successful_registration()
    def test_min_valid_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city=super().min_city_valid)
        super().assert_successful_registration()

class Street_Address(InitTest):
    def test_max_safe_street_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address=super().max_street_address_safe)
        super().assert_successful_registration()
    def test_max_valid_street_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address=super().max_street_address_valid)
        super().assert_successful_registration()
    def test_max_invalid_street_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address=super().max_street_address_invalid)
        super().assert_failed_registration()

    def test_min_safe_street_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address=super().min_street_address_safe)
        super().assert_successful_registration()
    def test_min_valid_street_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address=super().min_street_address_valid)
        super().assert_successful_registration()
