import unittest
import uuid
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support import expected_conditions as EC

class InitTest(unittest.TestCase):
    correct_username = "test"
    wrong_username = "'test'"
    correct_email = "test@test.com"
    wrong_email = "test.com"
    correct_password = "test"
    wrong_password = ""
    correct_password1 = "test"
    wrong_password1 = "somethingElse"
    correct_phone = "11223344"
    wrong_phone = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    correct_country = "Norway"
    wrong_country = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    correct_city = "Trondheim"
    wrong_city = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    correct_address = "Høyskoleringen 2"
    wrong_address = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    empty = None
    # initialization of webdriver 
    def setUp(self): 
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(chrome_options=chrome_options)

    # Helper function to find input
    def write_to_input(self, input_name, text):
        if (text == None):
            self.driver.find_element(By.NAME, input_name).send_keys(Keys.RETURN)
        else:
            self.driver.find_element(By.NAME, input_name).send_keys(text + Keys.RETURN)

    def write_inputs(
        self, 
        username = correct_username, 
        email = correct_email, 
        password = correct_password,
        password1 = correct_password1,
        phone = correct_phone,
        country = correct_country,
        city = correct_city,
        address = correct_address
    ):
        # This is needed to make sure duplicated usernames dont throw error
        if (username == self.correct_username):
            username = str(uuid.uuid4())
        self.write_to_input("username", username)
        self.write_to_input("email", email)
        self.write_to_input("password", password)
        self.write_to_input("password1", password1)
        self.write_to_input("phone_number", phone)
        self.write_to_input("country", country)
        self.write_to_input("city", city)
        self.write_to_input("street_address", address)
        self.submit()

    def assert_successful_registration(self):
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.title_is("Workouts"))

    def assert_failed_registration(self):
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.visibility_of_element_located((By.XPATH, "//strong[contains(text(),'Registration failed!')]")))

    # Helper function to sumbit form
    def submit(self):
        submit_button = self.driver.find_element(By.ID, "btn-create-account")
        submit_button.click()

    # distruction of webdriver
    def tearDown(self): 
        self.driver.close()

# ********** variables ********** 
# Username: 
#   - 1-150 characters
#       - Letters
#       - digits
#       - @/./+/-/_
#   - Unique
# Email:
#   - 0-254 characters
#       - valid email following Django's Regex
# Password:
#   - 1-infinity characters
#       - only 1 space is considered 0 characters
# Password1:
#   - Must be equal to Password
# Phone:
#   - 0-50 characters
# Country:
#   - 0-50 characters
# City:
#   - 0-50 characters
# Address:
#   - 0-50 characters
# ********** variables **********

# ********** states **********
# variable: wrong | correct | empty
# 
# Some variables are wrong if they are empty, thus empty => wrong
#
# Username: wrong | correct
# Email: wrong | correct | empty
# Password: wrong | correct
# Password1: wrong | correct
# Phone: Wrong | correct | empty
# Country: Wrong | correct | empty
# City: Wrong | correct | empty
# Address: Wrong | correct | empty
# ********** states **********

class UsernameEmail(InitTest): 
    def test_correct_username_correct_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_correct_username_empty_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_wrong_username_correct_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_username_empty_email(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, email = super().empty)
        super().assert_failed_registration()

class UsernamePassword(InitTest): 
    def test_correct_username_correct_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_username_correct_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, password = super().wrong_password)
        super().assert_failed_registration()

class UsernamePassword1(InitTest): 
    def test_correct_username_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_correct_username_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_username_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, password1 = super().wrong_password1)
        super().assert_failed_registration()

class UsernamePhone(InitTest): 
    def test_correct_username_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_correct_username_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    def test_wrong_username_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_username_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, phone = super().empty)
        super().assert_failed_registration()

class UsernameCountry(InitTest): 
    def test_correct_username_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_correct_username_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_wrong_username_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_username_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, country = super().empty)
        super().assert_failed_registration()

class UsernameCity(InitTest): 
    def test_correct_username_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_username_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_wrong_username_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_username_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, city = super().empty)
        super().assert_failed_registration()

class UsernameAddress(InitTest): 
    def test_correct_username_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_username_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_username_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_username_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username)
        super().assert_failed_registration()

    def test_wrong_username_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_username_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(username = super().wrong_username, address = super().empty)
        super().assert_failed_registration()

class EmailPassword(InitTest): 
    def test_correct_email_correct_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_email_wrong_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_email_correct_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, password = super().wrong_password)
        super().assert_failed_registration()

    def test_empty_email_correct_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_empty_email_wrong_password(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, password = super().wrong_password)
        super().assert_failed_registration()

class EmailPassword1(InitTest): 
    def test_correct_email_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()
    
    ''' Commented out to not break CI
    def test_correct_email_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_email_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, password1 = super().wrong_password1)
        super().assert_failed_registration()

    def test_empty_email_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_empty_email_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

class EmailPhone(InitTest): 
    def test_correct_email_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_email_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_correct_email_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_wrong_email_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_email_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, phone = super().empty)
        super().assert_failed_registration()

    def test_empty_email_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_empty_email_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_empty_email_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, phone = super().empty)
        super().assert_successful_registration()

class EmailCountry(InitTest): 
    def test_correct_email_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_email_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_correct_email_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_wrong_email_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_email_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, country = super().empty)
        super().assert_failed_registration()

    def test_empty_email_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_empty_email_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, country = super().wrong_country)
        super().assert_failed_registration()

    def test_empty_email_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, country = super().empty)
        super().assert_successful_registration()

class EmailCity(InitTest): 
    def test_correct_email_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_email_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_email_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_wrong_email_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_email_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, city = super().empty)
        super().assert_failed_registration()

    def test_empty_email_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_empty_email_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, city = super().wrong_city)
        super().assert_failed_registration()

    def test_empty_email_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, city = super().empty)
        super().assert_successful_registration()

class EmailAddress(InitTest): 
    def test_correct_email_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_email_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_email_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_email_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email)
        super().assert_failed_registration()

    def test_wrong_email_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_email_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().wrong_email, address = super().empty)
        super().assert_failed_registration()

    def test_empty_email_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty)
        super().assert_successful_registration()

    def test_empty_email_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, address = super().wrong_address)
        super().assert_failed_registration()

    def test_empty_email_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(email = super().empty, address = super().empty)
        super().assert_successful_registration()

class PasswordPassword1(InitTest): 
    def test_correct_password_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_correct_password_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_password_correct_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_password_wrong_password1(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, password1 = super().wrong_password1)
        super().assert_failed_registration()

class PasswordPhone(InitTest): 
    def test_correct_password_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_correct_password_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    def test_wrong_password_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_password_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_password_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, phone = super().empty)
        super().assert_failed_registration()

class PasswordCountry(InitTest): 
    def test_correct_password_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_correct_password_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_wrong_password_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_password_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_password_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, country = super().empty)
        super().assert_failed_registration()

class PasswordCity(InitTest): 
    def test_correct_password_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_password_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_wrong_password_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_password_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_password_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, city = super().empty)
        super().assert_failed_registration()

class PasswordAddress(InitTest): 
    def test_correct_password_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_password_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_password_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password)
        super().assert_failed_registration()

    def test_wrong_password_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_password_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password = super().wrong_password, address = super().empty)
        super().assert_failed_registration()

class Password1Phone(InitTest): 
    def test_correct_password1_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password1_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_correct_password1_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_correct_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_password1_wrong_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, phone = super().wrong_phone)
        super().assert_failed_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_empty_phone(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, phone = super().empty)
        super().assert_failed_registration()
    '''

class Password1Country(InitTest): 
    def test_correct_password1_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password1_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_correct_password1_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_password1_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, country = super().wrong_country)
        super().assert_failed_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, country = super().empty)
        super().assert_failed_registration()
    '''

class Password1City(InitTest): 
    def test_correct_password1_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password1_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_password1_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_password1_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, city = super().wrong_city)
        super().assert_failed_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, city = super().empty)
        super().assert_failed_registration()
    '''

class Password1Address(InitTest): 
    def test_correct_password1_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_password1_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_password1_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1)
        super().assert_failed_registration()
    '''

    def test_wrong_password1_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, address = super().wrong_address)
        super().assert_failed_registration()

    ''' Commented out to not break CI
    def test_wrong_password1_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(password1 = super().wrong_password1, address = super().empty)
        super().assert_failed_registration()
    '''

class PhoneCountry(InitTest): 
    def test_correct_phone_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_phone_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_correct_phone_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_wrong_phone_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_phone_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_phone_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, country = super().empty)
        super().assert_failed_registration()

    def test_empty_phone_correct_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    def test_empty_phone_wrong_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, country = super().wrong_country)
        super().assert_failed_registration()

    def test_empty_phone_empty_country(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, country = super().empty)
        super().assert_successful_registration()

class PhoneCity(InitTest): 
    def test_correct_phone_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_phone_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_phone_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_wrong_phone_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_phone_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_phone_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, city = super().empty)
        super().assert_failed_registration()

    def test_empty_phone_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    def test_empty_phone_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, city = super().wrong_city)
        super().assert_failed_registration()

    def test_empty_phone_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, city = super().empty)
        super().assert_successful_registration()

class PhoneAddress(InitTest): 
    def test_correct_phone_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_phone_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_phone_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_phone_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone)
        super().assert_failed_registration()

    def test_wrong_phone_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_phone_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().wrong_phone, address = super().empty)
        super().assert_failed_registration()

    def test_empty_phone_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty)
        super().assert_successful_registration()

    def test_empty_phone_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, address = super().wrong_address)
        super().assert_failed_registration()

    def test_empty_phone_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(phone = super().empty, address = super().empty)
        super().assert_successful_registration()

class CountryCity(InitTest): 
    def test_correct_country_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_country_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_correct_country_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_wrong_country_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_country_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country, city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_country_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country, city = super().empty)
        super().assert_failed_registration()

    def test_empty_country_correct_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_empty_country_wrong_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty, city = super().wrong_city)
        super().assert_failed_registration()

    def test_empty_country_empty_city(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty, city = super().empty)
        super().assert_successful_registration()

class CountryAddress(InitTest): 
    def test_correct_country_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_country_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_country_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_country_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country)
        super().assert_failed_registration()

    def test_wrong_country_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_country_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().wrong_country, address = super().empty)
        super().assert_failed_registration()

    def test_empty_country_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty)
        super().assert_successful_registration()

    def test_empty_country_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty, address = super().wrong_address)
        super().assert_failed_registration()

    def test_empty_country_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(country = super().empty, address = super().empty)
        super().assert_successful_registration()

class CityAddress(InitTest): 
    def test_correct_city_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs()
        super().assert_successful_registration()

    def test_correct_city_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().wrong_address)
        super().assert_failed_registration()

    def test_correct_city_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(address = super().empty)
        super().assert_successful_registration()

    def test_wrong_city_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city)
        super().assert_failed_registration()

    def test_wrong_city_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city, address = super().wrong_address)
        super().assert_failed_registration()

    def test_wrong_city_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().wrong_city, address = super().empty)
        super().assert_failed_registration()

    def test_empty_city_correct_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty)
        super().assert_successful_registration()

    def test_empty_city_wrong_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty, address = super().wrong_address)
        super().assert_failed_registration()

    def test_empty_city_empty_address(self):
        self.driver.get("http://localhost:3000/register.html")
        super().write_inputs(city = super().empty, address = super().empty)
        super().assert_successful_registration()
