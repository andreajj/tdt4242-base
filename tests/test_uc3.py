import unittest
import time
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

from .constants import DEFAULT_WAIT, TEST_ROOT, TEST_USER_USERNAME, TEST_USER_PASSWORD
from .helpers.registration import write_registration_inputs
from .helpers.login import log_in
from .helpers.toast import assert_toast, assert_not_toast

class test_uc3(unittest.TestCase): 
    # initialization of webdriver 
    def setUp(self): 
        # Configure driver options
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')

        self.driver = webdriver.Chrome(chrome_options=chrome_options)

        # Create the test user if it doesnt already exist
    
    def ensure_user_created(self, driver, username):
        driver.get("%s/register.html" % TEST_ROOT)
        write_registration_inputs(driver, 
            username, 
            "test@test.test", 
            TEST_USER_PASSWORD,
            TEST_USER_PASSWORD,
            "",
            "日本", # In case foreign characters
            "北海道",
            "マンホール通り")

    def fillExerciseForm(self, name, description, unit):
        self.driver.get("%s/exercise.html" % TEST_ROOT)
        nameComponent = WebDriverWait(self.driver, timeout=DEFAULT_WAIT).until(lambda d: d.find_element(By.ID, "inputName"))
        descriptionComponent = self.driver.find_element(By.ID, "inputDescription")
        unitComponent = self.driver.find_element(By.ID, "inputUnit")
        assert nameComponent
        assert descriptionComponent
        assert unitComponent
        nameComponent.send_keys(name)
        descriptionComponent.send_keys(description)
        unitComponent.send_keys(unit)
    """
        Rationale:

        This is an integration test of UC-3 "Show the user a notification when an action has succeeded or failed"

        As time is limited, we do not aim at 100% code coverage of all uses of the new toast function.
        Instead, we are using path-based integration testing.

        While i won't write the call graph here(see report?), following a toast through its whole life pretty much 
        ensures that most parts of the toast system is tested.

        Further, all toasts are shown due to the results of a rest api call. By testing both a success and a failure,
        we make sure all the mechanisms that are supposed to detect this, work, and that errors are passed to the toast
        properly.
    """
    def test(self):
        # Make sure the user is registered
        self.ensure_user_created(self.driver, "%s_toast" % TEST_USER_USERNAME)


        #################################################
        # Test integration between code that usesthe createToast function and, all toast features.

        # Sanity test: Toast works
        log_in(self.driver, "%s_toast" % TEST_USER_USERNAME, TEST_USER_PASSWORD)
        assert_toast(self.driver, "Welcome back!")
        # Wait 10 seconds and ensure the toast is still there
        time.sleep(10)
        assert_toast(self.driver, "Welcome back!")
        # Change page and make sure the toast is still there for another 10 seconds
        self.driver.get("%s/workouts.html" % TEST_ROOT)
        assert_toast(self.driver, "Welcome back!")
        time.sleep(13) # 3 seconds extra incase timing demons are mean
        assert_not_toast(self.driver, "Welcome back!")
        # Refresh, make sure the toast is still gone
        self.driver.get("%s/workouts.html" % TEST_ROOT)
        assert_not_toast(self.driver, "Welcome back!")

        ###############################################
        # Test a success toast. This also tests that that sendRequest's ok variable can be used to determine success
        # 
        self.fillExerciseForm("An exercise", "Good luck", "gangnam styles")
        submitBtn = self.driver.find_element(By.ID, "btn-ok-exercise")
        assert submitBtn
        submitBtn.click()
        assert_toast(self.driver, "Successfully created exercise")

        ###############################################
        # Test an error toast. This shows that errors from django are correctly detected.
        self.fillExerciseForm("An exercise", "", "")
        submitBtn = self.driver.find_element(By.ID, "btn-ok-exercise")
        assert submitBtn
        submitBtn.click()
        assert_toast(self.driver, "Could not create new exercise!")
        # Check that errors from django are correctly displayed
        description_error = self.driver.find_element(By.XPATH, "//*/li[contains(text(), 'description')]")
        assert description_error
        unit_error = self.driver.find_element(By.XPATH, "//*/li[contains(text(), 'unit')]")
        assert unit_error

        print("Done with toast test")

    def tearDown(self): 
        self.driver.close()