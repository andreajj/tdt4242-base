# Webserver running nginx
FROM nginx:perl

EXPOSE 80

# Copy nginx config to the container
COPY nginx.conf /etc/nginx/nginx.conf