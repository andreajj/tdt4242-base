FROM selenium/standalone-chrome

ENV DEBIAN_FRONTEND=noninteractive

RUN sudo apt-get update && sudo apt-get install -y screen python3-distutils python3-apt npm firefox
RUN sudo wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py
RUN python3 -m pip install selenium requests
RUN sudo npm install -g cordova

# Install geckodriver
RUN cd /tmp && wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz && tar -xvzf geckodriver* && chmod +x geckodriver && sudo mv geckodriver /usr/local/bin/ && sudo rm -r /tmp/*
