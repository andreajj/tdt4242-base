let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;
let oldImages;
let fileInput;
let illustrations;

function addIllustrations(files) {
    let inner = '';
    if (files.length === 0) {
        inner = 'No illustrations.';
    }
    illustrations.innerHTML = inner;
    for (let file of files) {
        const newLink = document.createElement('a');
        newLink.setAttribute('href', file.file);
        newLink.setAttribute('target', '_blank');
        newLink.style.padding = '0';
        newLink.classList.add('col-auto');
        newLink.style.objectFit = 'contain';
        newLink.style.mozBoxShadow = '0 0.0625em 0.125em rgba(0,0,0,0.15)';
        newLink.style.WebkitBoxShadow = '0 0.0625em 0.125em rgba(0,0,0,0.15)';

        const newImage = document.createElement('img');
        newImage.setAttribute('src', file.file);
        newImage.style.maxHeight = '150px';

        newLink.appendChild(newImage);
        illustrations.appendChild(newLink);
    }
}

function handleCancelButtonDuringEdit() {
    setReadOnly(true, '#form-exercise');
    okButton.classList.add('hide');
    deleteButton.classList.add('hide');
    cancelButton.classList.add('hide');
    editButton.classList.remove('hide');
    fileInput.classList.add('hide');

    cancelButton.removeEventListener('click', handleCancelButtonDuringEdit);

    let form = document.querySelector('#form-exercise');
    if (oldFormData.has('name')) form.name.value = oldFormData.get('name');
    if (oldFormData.has('description')) form.description.value = oldFormData.get('description');
    if (oldFormData.has('unit')) form.unit.value = oldFormData.get('unit');
    if (oldFormData.has('files')) {
        addIllustrations(oldImages);
        illustrations.classList.remove('hide');
    }

    oldFormData.delete('name');
    oldFormData.delete('description');
    oldFormData.delete('unit');
    oldFormData.delete('files');
}

function handleCancelButtonDuringCreate() {
    window.location.replace('exercises.html');
}

function createSubmitFormData() {
    let form = document.querySelector('#form-exercise');
    let formData = new FormData(form);
    let submitForm = new FormData();

    submitForm.append('name', formData.get('name'));
    submitForm.append('description', formData.get('description'));
    submitForm.append('unit', formData.get('unit'));

    for (let file of formData.getAll('files')) {
        submitForm.append('files', file);
    }

    return submitForm;
}

async function createExercise() {
    const submitForm = createSubmitFormData();

    let response = await sendRequest('POST', `${HOST}/api/exercises/`, submitForm, '');

    if (response.ok) {
        showToast('Success', { detail: 'Successfully created exercise', type: 'success' });
        window.location.replace('exercises.html');
    } else {
        let data = await response.json();
        showToast('Could not create new exercise!', { ...data, type: 'danger' });
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, '#form-exercise');

    editButton.classList.add('hide');
    okButton.classList.remove('hide');
    cancelButton.classList.remove('hide');
    deleteButton.classList.remove('hide');
    fileInput.classList.remove('hide');
    illustrations.classList.add('hide');

    cancelButton.addEventListener('click', handleCancelButtonDuringEdit);

    let form = document.querySelector('#form-exercise');
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    let response = await sendRequest('DELETE', `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        showToast('Could not delete exercise', { ...data, type: 'danger' });
    } else {
        showToast('Success', { detail: 'Successfully deleted the exercise', type: 'success' });
        window.location.replace('exercises.html');
    }
}

async function retrieveExercise(id) {
    let response = await sendRequest('GET', `${HOST}/api/exercises/${id}/`);
    console.log(response.ok);
    if (!response.ok) {
        let data = await response.json();
        showToast('Could not retrieve exercise data', { ...data, type: 'danger' });
    } else {
        let exerciseData = await response.json();
        let form = document.querySelector('#form-exercise');
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            if (key === 'files') {
                if (exerciseData[key] !== undefined) {
                    oldImages = exerciseData[key];
                    addIllustrations(exerciseData[key]);
                }
                continue;
            }
            let selector = `input[name="${key}"], textarea[name="${key}"]`;
            let input = form.querySelector(selector);
            input.value = exerciseData[key];
        }
        fileInput.classList.add('hide');
        illustrations.classList.remove('hide');
    }
}

async function updateExercise(id) {
    const submitForm = createSubmitFormData();

    let response = await sendRequest('PUT', `${HOST}/api/exercises/${id}/`, submitForm, '');

    if (!response.ok) {
        let data = await response.json();
        showToast(`Could not update exercise ${id}`, { ...data, type: 'danger' });
    } else {
        let responseData = await response.json();
        // duplicate code from handleCancelButtonDuringEdit
        // you should refactor this
        setReadOnly(true, '#form-exercise');
        okButton.classList.add('hide');
        deleteButton.classList.add('hide');
        cancelButton.classList.add('hide');
        editButton.classList.remove('hide');

        cancelButton.removeEventListener('click', handleCancelButtonDuringEdit);

        oldFormData.delete('name');
        oldFormData.delete('description');
        oldFormData.delete('unit');
        oldFormData.delete('files');
        oldImages = responseData['files'];
        addIllustrations(oldImages);
        fileInput.classList.add('hide');
        illustrations.classList.remove('hide');

        showToast('Success', { detail: 'Successfully updated exercise', type: 'success' });
    }
}

window.addEventListener('DOMContentLoaded', async () => {
    cancelButton = document.querySelector('#btn-cancel-exercise');
    okButton = document.querySelector('#btn-ok-exercise');
    deleteButton = document.querySelector('#btn-delete-exercise');
    editButton = document.querySelector('#btn-edit-exercise');
    oldFormData = null;
    fileInput = document.querySelector('#inputFile');
    illustrations = document.querySelector('#illustrations');

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveExercise(exerciseId);

        editButton.addEventListener('click', handleEditExerciseButtonClick);
        deleteButton.addEventListener('click', (async (id) => await deleteExercise(id)).bind(undefined, exerciseId));
        okButton.addEventListener('click', (async (id) => await updateExercise(id)).bind(undefined, exerciseId));
    }
    //create
    else {
        setReadOnly(false, '#form-exercise');
        fileInput.classList.remove('hide');

        editButton.classList.add('hide');
        okButton.classList.remove('hide');
        cancelButton.classList.remove('hide');

        okButton.addEventListener('click', async () => await createExercise());
        cancelButton.addEventListener('click', handleCancelButtonDuringCreate);
    }
});
