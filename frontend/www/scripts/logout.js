deleteCookie('access');
deleteCookie('refresh');
deleteCookie('remember_me');
sessionStorage.removeItem('username');

showToast('Success', { detail: 'Logout complete, welcome back later!', type: 'success' });

window.location.replace('index.html');
